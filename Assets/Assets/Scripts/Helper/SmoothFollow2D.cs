using UnityEngine;
using System.Collections;

public class SmoothFollow2D : MonoBehaviour{

	private GameObject addPlayer;
	public float smoothTime;
	public float offsetX;
	public float offsetY;
	
	private float positionX ;
	private float positionY;
	private float positionZ;
	
	private Transform thisTransform;
	private Vector2 velocity;

	void Start(){
		thisTransform = transform;
		
	}

	void LateUpdate(){
		
		addPlayer = GameObject.FindWithTag("Player");
		
		if (addPlayer) {
			SmoothFollow();
		}
	}
	
	public void SmoothFollow(){
		positionX = Mathf.SmoothDamp( thisTransform.position.x, addPlayer.transform.position.x + offsetX, ref velocity.x, smoothTime);
		positionY = Mathf.SmoothDamp( thisTransform.position.y, addPlayer.transform.position.y + offsetY, ref velocity.y, smoothTime);

		positionZ = thisTransform.position.z;
		thisTransform.position = new Vector3 (positionX, positionY, positionZ);

	}
}