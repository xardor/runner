using UnityEngine;
using System.Collections;

public class MenuAnim : MonoBehaviour {

	public int 		moveToValue = 0;
	public int 		moveFromValue = 0;
	public string 	easetype = "linear";
	public int 		time = 2;
	public float 	delay = 0.5f;
	public bool 	islocal = true;
	public bool 	ignoreTimeScale = false;
	
	public bool		pauseiTween = true;
	
	private bool isGoActive = false;
	
	
	void Awake(){

	}
	
	// Use this for initialization
	void Start () {
		if (pauseiTween) {
			iTween.Pause(gameObject);
		}
		isGoActive = true;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (enabled && isGoActive) {
			iTween.Resume(gameObject);
			isGoActive = false;
			iTween.MoveTo(gameObject,iTween.Hash("y",moveToValue,
						 			 "easetype",easetype,
						 			 "time",time,
						 			 "delay",delay,
									 "islocal",islocal,
									 "looptype",iTween.LoopType.none,
									 "ignoretimescale",ignoreTimeScale));
		}
	}
}
