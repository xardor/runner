using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	
	private RaycastHit hit;
	private float distance = 200.0f;
	
//	public bool toggleGameStart = false;
	
	private GameObject mainGameCam;
	private GameObject hudCam;
	private GameObject mainMenuCam;
	private GameObject pauseMenuCam;
	private GameObject gameOverCam;
	private GameObject highScoreMenuCam;
	
	public GameObject menuSprite;
	
//	public float aniStartPos, aniEndPos;
	
//	public exScreenPosition exScreenPos;
	public PlayerManager pm;
	
	public bool toggleHighScore;
	
	void Awake(){
//		iTween.MoveTo(menuSprite,iTween.Hash("y",110,"easetype","easeOutElastic","time",2,"delay",0.5,"islocal",true,"looptype",iTween.LoopType.none));
	}
	
	// Use this for initialization
	void Start () {
		
//		GameStateManager.GameLoad  += GameLoad;
		GameStateManager.GameStart += GameStart;
		GameStateManager.GameQuit  += GameQuit;
		GameStateManager.HighScoreMenu += HighScoreMenu;
		
		mainGameCam		 = GameObject.FindGameObjectWithTag("MainCamera");
		mainMenuCam  	 = GameObject.Find("MainMenuCamera");
		hudCam		 	 = GameObject.Find("HudCamera");
		pauseMenuCam 	 = GameObject.Find("PauseMenuCamera");
		gameOverCam	 	 = GameObject.Find("GameOverCamera");
		highScoreMenuCam = GameObject.Find("HighScoreMenuCamera");
		
		
		mainMenuCam.SetActiveRecursively(true);
		mainGameCam.SetActiveRecursively(true);
		hudCam.SetActiveRecursively(false);
		pauseMenuCam.SetActiveRecursively(false);
		gameOverCam.SetActiveRecursively(false);
		highScoreMenuCam.SetActiveRecursively(false);
	}
	
	// Update is called once per frame
	void Update () {
 
//		GameStateManager.TriggerGameLoad();
		
		if (toggleHighScore) {
			toggleHighScore = false;
			GameStateManager.TriggerHighScoreMenu();
		}
		
		
		if (Input.GetKeyDown("return")) {
			GameStateManager.TriggerGameStart();
		}
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				
				Ray ray = mainMenuCam.camera.ScreenPointToRay(touch.position);

				if (Physics.Raycast(ray, out hit, distance)) {
					
					if (hit.transform.name == "StartGameText") {
						GameStateManager.TriggerGameStart();	
					}

					if (hit.transform.name == "HighScoreText") {
						GameStateManager.TriggerHighScoreMenu();
					}
					
					if (hit.transform.name == "QuitGameText") {
						GameStateManager.TriggerGameQuit();
					}
				}
			}
		}
		
	}
	
//	private void GameLoad(){
//		
//		exScreenPos.y = Mathfx.Berp(aniStartPos,aniEndPos,Time.realtimeSinceStartup);
//	}
	
	private void GameStart(){
		mainGameCam.SetActiveRecursively(true);
		mainMenuCam.SetActiveRecursively(false);
		pauseMenuCam.SetActiveRecursively(false);
		gameOverCam.SetActiveRecursively(false);
		hudCam.active = true;
		pm.Spawn();
//		Debug.Log("Game is starting!");
	}
	
	private void HighScoreMenu(){
		mainMenuCam.SetActiveRecursively(false);
		highScoreMenuCam.SetActiveRecursively(true);
//		Debug.Log("Activated HighScoreMenu");
	}
	
	private void GameQuit(){
		Application.Quit ();
	}
}