using UnityEngine;
using System.Collections;

public class HighScoreMenu : MonoBehaviour{
	
	private RaycastHit hit;
	private Ray ray;
	private float distance = 200.0f;
	
	private GameObject highScoreMenuCam;
//	private GameObject mainMenuCam;
	
	// Use this for initialization
	void Start (){
	
		highScoreMenuCam = GameObject.Find("HighScoreMenuCamera");
//		mainMenuCam  	 = GameObject.Find("MainMenuCamera");
	}
	
	// Update is called once per frame
	void Update (){
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				
				ray = highScoreMenuCam.camera.ScreenPointToRay(touch.position);

				if (Physics.Raycast(ray, out hit, distance)) {
					
					if (hit.transform.name == "BackButton") {
						GameStateManager.TriggerGameReset();	
					}
				}
			}
		}
	}
}

