using UnityEngine;
using System.Collections;

public class GameOverMenu : MonoBehaviour{
	
	private RaycastHit hit;
	private float distance = 200.0f;
	
	private GameObject gameOverCam;
	private GameObject hudCam;
	public exSpriteFont playerDistanceText;
	public HighScore Score;
	
	public bool toggleGameRestart = false;
	
	// Use this for initialization
	void Start (){
		
		GameStateManager.GameOver += GameOver;
		
		gameOverCam	 = GameObject.Find("GameOverCamera");
		hudCam		 = GameObject.Find("HudCamera");
	}
	
	// Update is called once per frame
	void Update (){
		
		if (toggleGameRestart) {
			GameStateManager.TriggerGameReset();
			toggleGameRestart = false;
		}
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				
				Ray ray = gameOverCam.camera.ScreenPointToRay(touch.position);

				if (Physics.Raycast(ray, out hit, distance)) {
					
					if (hit.transform.name == "RestartText") {
						GameStateManager.TriggerGameReset();
					}
				}
			}
		}
	}
	
	private void GameOver(){
		
		int distance = (int)PlayerManager.distanceTraveled/10;
		
		playerDistanceText.text = System.String.Format("You got {0:f0} m far!",distance);
	
		Score.SaveHighScore("New Player",distance);

		gameOverCam.SetActiveRecursively(true);
		hudCam.SetActiveRecursively(false);
		Destroy(GameObject.FindWithTag("Player"));
	}
}

