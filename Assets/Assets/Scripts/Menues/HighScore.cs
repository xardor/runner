using UnityEngine;
using System.Collections.Generic;

public class HighScore : MonoBehaviour{
	
	private exSpriteFont hScore;
	
	List<Scores> _scores;
	
	// Use this for initialization
	void Start (){
	
		hScore = GetComponent<exSpriteFont>();
		
		_scores = new List<Scores> ();
		_scores = GetHighScore ();
		
		hScore.text = "";
		
		hScore.text = System.String.Format("Name: " + "              " + "Score:"+"\n \n");
		
		for (int i=0; i < _scores.Count; i++) {
			hScore.text += System.String.Format(_scores [i].name + ".............." + _scores [i].score+"\n");
		}
	}
	
	// Update is called once per frame
	void Update (){
//		SaveHighScore("New Player", (int)Random.Range (1, 9999));
	}

	public void SaveHighScore (string name, int score){
	
		List<Scores> HighScores = new List<Scores> ();
	
		int i = 1;
		while (i <= 10 && PlayerPrefs.HasKey("HighScore"+i+"score")) {
			
			Scores temp = new Scores ();
			temp.score = PlayerPrefs.GetInt ("HighScore" + i + "score");
			temp.name = PlayerPrefs.GetString ("HighScore" + i + "name");
			HighScores.Add (temp);
			i++;
		}
		
		if (HighScores.Count == 0) {
			
			Scores _temp = new Scores ();
			_temp.name = name;
			_temp.score = score;
			HighScores.Add (_temp);
			
		} else {
			//Insert new score
			for (i = 1; i <= HighScores.Count && i <= 10; i++) {
				if (score > HighScores [i - 1].score) {
					Scores _temp = new Scores ();
					_temp.name = name;
					_temp.score = score;
					HighScores.Insert (i - 1, _temp);
					break;
				}
			}
		}
		
			i = 1;
			while (i <= 10 && i <= HighScores.Count) {
				
				PlayerPrefs.SetString ("HighScore" + i + "name", HighScores [i - 1].name);
				PlayerPrefs.SetInt ("HighScore" + i + "score", HighScores [i - 1].score);
				i++;
		}
	}
	
	List<Scores>  GetHighScore (){
	
		List<Scores> HighScores = new List<Scores> ();
	
		int i = 1;
		while (i<=10 && PlayerPrefs.HasKey("HighScore"+i+"score")) {
			Scores temp = new Scores ();
			temp.score = PlayerPrefs.GetInt ("HighScore" + i + "score");
			temp.name = PlayerPrefs.GetString ("HighScore" + i + "name");
			HighScores.Add (temp);
			i++;
		}
		
		return HighScores;
	}
}
	
class Scores{
	public int score;
	public string name;
}