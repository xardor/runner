using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	private RaycastHit hit;
	
	private Ray rayPause;
	private float distance = 200.0f;
	
	private float savedTimeScale;
	
	private GameObject hudCam;
	private GameObject pauseMenuCam;
	private GameObject mainMenuCam;
	private GameObject gameOverCam;
	private GameObject pauseButton;
	private GameObject backButton;
	private GameObject highScoreCam;
	
	public PlayerManager pm;
	
	public bool toggleGameResume = false;
	public bool toggleGameReset	 = false;
	
	private bool isGamePaused;

	// Use this for initialization
	void Start () {
		
		GameStateManager.GamePause  += GamePause;
		GameStateManager.GameReset  += GameReset;
		GameStateManager.GameResume += GameResume;
		
		highScoreCam = GameObject.Find("HighScoreMenuCamera");
		hudCam		 = GameObject.Find("HudCamera");
		mainMenuCam  = GameObject.Find("MainMenuCamera");
		pauseMenuCam = GameObject.Find("PauseMenuCamera");
		gameOverCam  = GameObject.Find("GameOverCamera");
		pauseButton	 = GameObject.Find("HUDPauseButton");
		backButton	 = GameObject.Find("BackButton");
	}
	
	// Update is called once per frame
	void Update () {

		if (toggleGameResume) {
			toggleGameResume = false;
			GameStateManager.TriggerGameResume();
		}
//		
		if (toggleGameReset) {
			toggleGameReset = false;
			GameStateManager.TriggerGameReset();
		}
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {

				rayPause = pauseMenuCam.camera.ScreenPointToRay(touch.position);
				
				if(Physics.Raycast(rayPause, out hit, distance)){
					
					if (hit.transform.name == "MainMenuText") {
						GameStateManager.TriggerGameReset();
					}
					
					if (hit.transform.name == "ResumeGameText") {
						GameStateManager.TriggerGameResume();
					}
				}
			}
		}
	}

	private void GamePause(){
		if (!isGamePaused) {
			isGamePaused = true;
			savedTimeScale = Time.timeScale;
			Time.timeScale = 0.0f;
			pauseMenuCam.SetActiveRecursively(true);
			mainMenuCam.SetActiveRecursively(false);
			AudioListener.pause = true;
			hudCam.active = false;
//			Debug.Log("Game is paused!");	
		}
	}
	
	private void GameReset(){
		
		if (isGamePaused) {
			isGamePaused = false;
			Time.timeScale = savedTimeScale;
			AudioListener.pause = false;
			pauseButton.active = true;
		}
		
		
		mainMenuCam.SetActiveRecursively(true);
		highScoreCam.SetActiveRecursively(false);
		pauseMenuCam.SetActiveRecursively(false);
		gameOverCam.SetActiveRecursively(false);
		hudCam.active = false;
		backButton.active = false;
		Destroy(GameObject.FindWithTag("Player"));
//		Debug.Log("Game is unpaused and we are back at the main menu!");
	}
	
	private void GameResume(){
		
		if (isGamePaused) {
			isGamePaused = false;
			Time.timeScale = savedTimeScale;
			AudioListener.pause = false;
		}

		pauseMenuCam.SetActiveRecursively(false);
		hudCam.active = true;
//		Debug.Log("Game is resumed!");
	}
	
	
}