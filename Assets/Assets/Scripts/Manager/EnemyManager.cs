using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour {

	public PlayerManager pm;
	public GameObject explosion;
	public GameObject drone;
	public float recycleOffset;
	
	void Awake(){
		iTween.MoveBy(drone,iTween.Hash("y",10,"easetype","easeinoutcubic","time",0.5,"delay",0,"islocal",true,"looptype",iTween.LoopType.pingPong));
	}
	
	// Use this for initialization
	void Start () {
		drone = this.gameObject;
		
		GameStateManager.GameStart += GameStart;
		GameStateManager.GameReset += GameReset;
	}
	
	private void GameStart(){
		if(!drone.renderer.enabled) {
			drone.collider.enabled = true;
			drone.renderer.enabled = true;
		}
	}
	
	private void GameReset(){
		if(!drone.renderer.enabled) {
			drone.collider.enabled = true;
			drone.renderer.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.localPosition.x + recycleOffset < PlayerManager.distanceTraveled && !drone.renderer.enabled){
			drone.renderer.enabled = true;
			drone.collider.enabled = true;
//			Debug.Log("Drone reactivated!");
			return;
		}			
	}
	
	//Instantiate an explosion and disable the renderer and collider of the drone.
	//We do this to reactivate the components trough Update() for being able to
	//reuse them through the ObjectManager.
	void OnTriggerEnter(Collider collider) {
		
		if (collider.tag == "Player") {
			pm.Hurt();
		}
		
		Instantiate(explosion,this.transform.position,Quaternion.identity);
		drone.renderer.enabled = false;
		drone.collider.enabled = false;
//		Debug.Log("Got hit by drone!");
	}

}
