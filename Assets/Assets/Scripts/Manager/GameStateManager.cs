public static class GameStateManager{

	public delegate void GameState();
	
	public static event GameState GameLoad, GameReset, GameStart, GameOver, GamePause, GameResume, GameQuit, HighScoreMenu;
	
	public static void TriggerGameLoad(){
		if(GameLoad != null){
			GameLoad();
		}
	}
	
	public static void TriggerGameStart(){
		if(GameStart != null){
			GameStart();
		}
	}
	
	public static void TriggerGameOver(){
		if(GameOver != null){
			GameOver();
		}
	}
	
	public static void TriggerGameQuit(){
		if (GameQuit != null) {
			GameQuit();
		}
	}
	
	public static void TriggerGamePause(){
		if (GamePause != null) {
			GamePause();
		}
	}
	
	public static void TriggerGameResume(){
		if (GameResume != null) {
			GameResume();
		}
	}
	
	public static void TriggerGameReset(){
		if (GameReset != null) {
			GameReset();
		}
	}
	
	public static void TriggerHighScoreMenu(){
		if (GameReset != null) {
			HighScoreMenu();
		}
	}
}
