using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class AudioManager : MonoBehaviour{
	
	public bool playAudio = false;
	
	public GameObject player;	
	private CharacterController charController;
	
	public AudioClip	jumpSoundFile;
	public AudioClip[]	runningSoundFile;
	
	private float timeUntilNextStepSound = 0.0f;
	public float CONST_STEP_INTERVAL = 0.3f;
	
	// Use this for initialization
	void Start (){
	}
	
	// Update is called once per frame
	void Update () {
	
		player = GameObject.FindGameObjectWithTag("Player");
		
		if (player) {
			charController = player.GetComponent<CharacterController>();
			playAudio = true;
		}else if(!player){
			playAudio = false;
		}

		if (playAudio) {
			//Timer
			timeUntilNextStepSound -= Time.deltaTime;
			
			if (charController.isGrounded) {	
				PlayStepSounds();	
	
				}else if (!charController.isGrounded){
					audio.Pause();
				}
			}else if(!playAudio){
				audio.Stop ();
			}
	}
	
	void PlayStepSounds(){
		if (timeUntilNextStepSound <= 0.0f) {
			timeUntilNextStepSound = CONST_STEP_INTERVAL;
			audio.clip = runningSoundFile[Random.Range(0,runningSoundFile.Length)];
			audio.PlayOneShot(audio.clip);
		}
	}
	
	public void PlayJumpSound(){
		audio.clip = jumpSoundFile;
		audio.PlayOneShot(audio.clip);
	}
}

