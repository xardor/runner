using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class PlayerManager : MonoBehaviour {
		
	public	static float	distanceTraveled;
	
	public	float			actualSpeed		 = 0.0f;
	public  float			maxSpeed		 = 230.0f;
	public  float 			jumpSpeed		 = 150.0f;
    public  float			worldGravity	 = 250.0f;
	public  float			acceleration	 = 0.5f;
	private float			smoothAccel		 = 0.3f;
	private Vector3			moveDirection	 = Vector3.zero;
	
	[HideInInspector]
	public  int			lives;
	private Vector3		spawnPoint;
	
	private CharacterController playerController;
	private  AudioManager		audioMgr;
	
	public  exSprite playerSprite;
	
	public 	bool	isJumping;
	public 	bool	isRunning;

	private RaycastHit	hit;
	private Ray			ray;
	private float		rayDistance = 200.0f;
	private GameObject	hudCam;

	// Use this for initialization
	void Start () {
		
		isJumping = false;
		isRunning = true;
		
		playerController = GetComponent<CharacterController>();
		playerSprite	 = GetComponent<exSprite>();
		audioMgr		 = GetComponent<AudioManager>();
		
		hudCam	 = GameObject.FindGameObjectWithTag("HUDCamera");
	}

	// Update is called once per frame	
	void Update () {

		Run();
		Acceleration();
		
		//Make sure we can't double jump
		if (playerController.isGrounded){
			
			//moveDirection = this.transform.right * actualSpeed;
			moveDirection = new Vector3(1,0,0) * actualSpeed;
			
			//Get touch input
			foreach (Touch touch in Input.touches) {
				if (touch.phase == TouchPhase.Began) {
					
					ray = hudCam.camera.ScreenPointToRay(touch.position);
					if (Physics.Raycast(ray, out hit, rayDistance)) {

						if (hit.transform.name == "JumpButton") {
							isJumping = true;
						}
					}
				}
			}
			
			//get keyboard input
			if (Input.GetKeyDown("up")){
				isJumping = true;
			}
			
			if (isJumping) {
				Jump ();
				
			}
			
		}else if (!playerController.isGrounded) {
			playerSprite.spanim.Play("playerRun");
		}

		// Apply gravity
		moveDirection.y -= worldGravity * Time.deltaTime;
		
		distanceTraveled = this.transform.localPosition.x;
	}
	
	public void Acceleration(){
		smoothAccel = acceleration * Time.deltaTime;
		actualSpeed = Mathf.Lerp(actualSpeed,maxSpeed,smoothAccel);
	}
	
	public void Run(){
		playerController.Move (moveDirection * Time.deltaTime);	
	}
	
	public void Jump(){
		isJumping = false;
		moveDirection.y = jumpSpeed;
		audioMgr.PlayJumpSound();
	}
	
	public void Spawn(){
		lives = 3;
		spawnPoint = new Vector3(0,30,0);
		Instantiate(this,spawnPoint,Quaternion.identity);
	}
	
	public void Hurt(){
		lives--;
		if(lives <= 0) {
			GameStateManager.TriggerGameOver();
        }
	}
}