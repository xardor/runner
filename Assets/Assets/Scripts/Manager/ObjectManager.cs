using UnityEngine;
using System.Collections.Generic;

public class ObjectManager : MonoBehaviour {
	
	public GameObject prefab;
	public int numberOfObjects;
	public int minDistance, maxDistance;
	public float recycleOffset;

	private Vector3 nextPosition;
	public Queue<GameObject> objectQueue;
	
	
	
	void Start () {
		
		GameStateManager.GameStart += GameStart;
		GameStateManager.GameReset += GameReset;
		
		objectQueue = new Queue<GameObject>(numberOfObjects); //Create FIFO ObjectQueue with the respective size
		
		for(int i = 0; i < numberOfObjects; i++){
			objectQueue.Enqueue((GameObject)Instantiate(prefab)); //Create number of objects and put them in queue
		}

		nextPosition = transform.localPosition; 
		
		for(int i = 0; i < numberOfObjects; i++){
			RecycleObjects();
		}
	}
	
	void Update () {
		if (objectQueue != null) {
			if(objectQueue.Peek().transform.localPosition.x + recycleOffset < PlayerManager.distanceTraveled){
				RecycleObjects();
			}
		}
	}
	
	private void GameStart () {
		nextPosition = transform.localPosition;
		for(int i = 0; i < numberOfObjects; i++){
			RecycleObjects();
		}
	}
	
	private void GameReset () {
		nextPosition = transform.localPosition;
		for(int i = 0; i < numberOfObjects; i++){
			RecycleObjects();
		}
	}
	
	public void RecycleObjects(){
				
		GameObject obj = objectQueue.Dequeue();		//Get the oldest object out of queue
		
		float getScale = obj.GetComponent<exSprite>().scale.x;
		float getWidth = obj.GetComponent<exSprite>().width;
		float spriteSize = getScale * getWidth;

		obj.transform.localPosition = nextPosition; //Set position of object
		nextPosition.x += spriteSize + Random.Range(minDistance, maxDistance) ;
		objectQueue.Enqueue(obj);
	}
}