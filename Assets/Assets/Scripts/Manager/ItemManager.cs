using UnityEngine;
using System.Collections;

public class ItemManager : MonoBehaviour {

	public PlayerManager pm;
	public GameObject heartItem;
	public float recycleOffset;
	public AudioClip pickUpSound;
	
	// Use this for initialization
	void Start () {
		heartItem = this.gameObject;
		
		GameStateManager.GameStart += GameStart;
		GameStateManager.GameReset += GameReset;
	}
	
	private void GameStart(){
		if(!heartItem.renderer.enabled && !heartItem.collider.enabled) {
			heartItem.renderer.enabled = true;
			heartItem.collider.enabled = true;
		}
	}
	
	private void GameReset(){
		if(!heartItem.renderer.enabled && !heartItem.collider.enabled) {
			heartItem.renderer.enabled = true;
			heartItem.collider.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.localPosition.x + recycleOffset < PlayerManager.distanceTraveled && !heartItem.renderer.enabled){
			heartItem.renderer.enabled = true;
			heartItem.collider.enabled = true;
//			Debug.Log("Heart reactivated!");
			return;
		}
	}
	
	void OnTriggerEnter(Collider collider) {
		if (collider.tag == "Player") {
			heartItem.renderer.enabled = false;
			heartItem.collider.enabled = false;
			audio.PlayOneShot(pickUpSound);
			pm.lives++;
		}
//		Debug.Log("Live +1!");
	}
}
