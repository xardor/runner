using UnityEngine;
using System.Collections;

public class PhysicsManager : MonoBehaviour {
 
	public PlayerManager pm;
	public GameObject explosion;
	public GameObject crate;
	public float recycleOffset;
	
	// Use this for initialization
	void Start () {

		crate = this.gameObject;
		
		GameStateManager.GameStart += GameStart;
		GameStateManager.GameReset += GameReset;
	}
	
	private void GameStart(){
		if(!crate.renderer.enabled) {
			crate.collider.enabled = true;
			crate.renderer.enabled = true;
		}
	}
	
	private void GameReset(){
		if(!crate.renderer.enabled) {
			crate.collider.enabled = true;
			crate.renderer.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.localPosition.x + recycleOffset < PlayerManager.distanceTraveled && !crate.renderer.enabled){
			crate.renderer.enabled = true;
			crate.collider.enabled = true;
//			Debug.Log("Crate reactivated!");
			return;
		}
	}
	
	//Instantiate an explosion and disable the renderer and collider of the crate.
	//We do this to reactivate the components trough Update() for being able to
	//reuse them trough the ObjectManager.
	void OnTriggerEnter(Collider collider) {
		
		if (collider.tag == "Player") {
			Instantiate(explosion,this.transform.position,Quaternion.identity);
			crate.renderer.enabled = false;
			crate.collider.enabled = false;
			pm.Hurt();
		}
		
//		Debug.Log("Hit Crate!");
	}
}
