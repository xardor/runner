using UnityEngine;
using System.Collections;

public class HUDManager : MonoBehaviour {
	
	// Bit shift the index of the layer (8) [HUD] to get a bit mask
    int layerMask = 1 << 8;
	

	private RaycastHit hit;
	private float distance = 200.0f;
	
	private GameObject hudCam;
	
	public exSpriteFont fontDistance;
	public exSpriteFont fontHealth;
	private float playerDistance;
	
	public PlayerManager pm;
	
	public bool toggleGamePause	 = false;
	
	// Use this for initialization
	void Start () {
		hudCam		 = GameObject.Find("HudCamera");
	}
	
	// Update is called once per frame
	void LateUpdate () {
		
//		if (Input.GetKeyDown("escape")) {
//			if(toggleGamePause){
//				toggleGamePause = false;
//				GameStateManager.TriggerGamePause ();
//			}else {
//				toggleGamePause = true;
//			}
//		}
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				
				//Send out a ray on touch position.
				Ray rayHud = hudCam.camera.ScreenPointToRay(touch.position);

				if (Physics.Raycast(rayHud, out hit, distance, layerMask)) {//, layerMask)) {
					
					if (hit.transform.name == "HUDPauseButton") {
						GameStateManager.TriggerGamePause();
					}
				}		
			}
		}
		
		DrawGuiDistance();
		DrawHealthInfo();
	}
	
	void DrawGuiDistance(){
		
		playerDistance = PlayerManager.distanceTraveled/10;
		
		if (fontDistance == null) {
			Debug.Log("No Object instantiated!");
		}else if (fontDistance) {
			string format = System.String.Format("{0:f0} m",playerDistance);
			fontDistance.text = format;
		}
		
	}
	
	void DrawHealthInfo(){
		string format = System.String.Format ("x {0:f0}", pm.lives);
		fontHealth.text = format;
	}
}
