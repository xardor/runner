using UnityEngine;
using System.Collections;

public class LaserBeam : MonoBehaviour {
	
	private float speed = 600.0f;
	private float timeToLive = 0.85f;
	private float expireTime = 0.0f;
	public AudioClip LaserBeamSound;
	
	public delegate void BeamDiedEvent();
	public event BeamDiedEvent BeamDied;
	
	void Start(){
		expireTime = Time.time + timeToLive;
		audio.PlayOneShot(LaserBeamSound);
	}
	
	void Update(){
		if(Time.time > expireTime){
			KillBeam();
		}
		
		transform.Translate(Vector3.right * speed * Time.deltaTime);
	}
	
	void OnTriggerEnter(Collider collider){
		
		if(collider.tag == "Enemy"){
			KillBeam();
		}
		
		if(collider.tag == "Crate"){
			KillBeam();
		}
	}

	void KillBeam(){
		Destroy(gameObject);
		BeamDied();
	}
}

