using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
	
	private float timeToLive = 3.0f;
	private float expireTime = 0.0f;
	public AudioClip Sound;
	
	// Use this for initialization
	void Start () {
		expireTime = Time.time + timeToLive;
		audio.PlayOneShot(Sound);
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Time.time > expireTime){
			Destroy(gameObject);
		}
	}
	
	
}
