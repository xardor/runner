using UnityEngine;
using System.Collections;

public class LaserWeapon : MonoBehaviour {
	
	private float maxBeams = 3;
	private float currentBeams = 0;
	
	public LaserBeam LaserBeamPrefab;
	
	
	private RaycastHit	hit;
	private Ray			ray;
	private float		rayDistance = 200.0f;
	
	private GameObject	hudCam;
	
	void Start(){
		hudCam	 = GameObject.FindGameObjectWithTag("HUDCamera");
	}
	
	void Update(){
		
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				
				ray = hudCam.camera.ScreenPointToRay(touch.position);
				
				if (Physics.Raycast(ray, out hit, rayDistance)) {
					if (hit.transform.name == "ShootButton") {
						if (currentBeams < maxBeams) {
							Fire();
						}
					}
				}
			}
		}
		
		if (Input.GetKeyDown("space") && currentBeams < maxBeams) {
			Fire();
		}		
	}
	
	void Fire(){
		
		currentBeams++;
		
		LaserBeamPrefab.transform.position = transform.position;
		LaserBeamPrefab.transform.rotation = transform.rotation;

//		float offset = (transform.localScale.y / 2.0f) + (LaserBeamPrefab.transform.localScale.y / 2.0f);
//		LaserBeamPrefab.transform.Translate(Vector3.right * offset);
		
		LaserBeam laserBeam = (LaserBeam)Instantiate(LaserBeamPrefab);

		laserBeam.BeamDied += CurrentBeams;
		
	}
	
	void CurrentBeams(){
		this.currentBeams--;
	}
}
